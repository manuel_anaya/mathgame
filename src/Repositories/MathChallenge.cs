﻿using System;
using System.Collections.Generic;
using MathGame.Repositories;

namespace MathGame.Models
{
    public class MathChallenge : IMathChallenge
    {
        public MathChallenge()
        {
            Answers = new List<Tuple<string, string, bool>>();
        }
        public string ChallengeId { get; set; }

        public string Predicate { get; set; }

        public bool RightAnswer { get; set; }

        public string ChallengeWinner { get; set; }

        public List<Tuple<string, string, bool>> Answers { get; set; }

        public void RefreshChallenge(string id, string predicate, bool rightAnswer)
        {
            ChallengeId = id;
            Predicate = predicate;
            RightAnswer = rightAnswer;
            ChallengeWinner = string.Empty;
            Answers.Clear();
        }

    }
}