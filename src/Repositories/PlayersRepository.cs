﻿using System.Collections.Generic;
using System.Linq;
using MathGame.Models;

namespace MathGame.Repositories
{
    public class PlayersRepository : IPlayersRepository
    {
        private List<Player> _players = new List<Player>();

        public Player Add(Player player)
        {
            lock (_players)
            {
                if(_players.Count <= 10)
                {
                    _players.Add(player);
                }                
                return player;
            }            
        }

        public void Delete(string connectionId)
        {
            lock (_players)
            {
                var player = GetPlayer(connectionId);
                if (player != null)
                {
                    _players.Remove(player);
                }
            }
        }

        public Player GetPlayer(string connectionId)
        {
            return _players.FirstOrDefault(x => x.ConnectionId == connectionId);
        }

        public List<Player> GetPlayers()
        {
            return _players;
        }

        public void ResetJustAdded()
        {
            lock (_players)
            {
                _players.ForEach(x => x.JustAdded = false);
            }
        }
    }
}
