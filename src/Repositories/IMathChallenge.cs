﻿using System;
using System.Collections.Generic;

namespace MathGame.Repositories
{
    public interface IMathChallenge
    {
        string ChallengeId { get; set; }

        string Predicate { get; set; }

        bool RightAnswer { get; set; }

        string ChallengeWinner { get; set; }

        //bool HasWinner { get; }

        List<Tuple<string, string, bool>> Answers { get; set; }

        void RefreshChallenge(string id, string predicate, bool rightAnswer);
    }
}
