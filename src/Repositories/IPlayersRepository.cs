﻿using MathGame.Models;
using System.Collections.Generic;

namespace MathGame.Repositories
{
    public interface IPlayersRepository
    {
        List<Player> GetPlayers();        
        Player GetPlayer(string connectionId);
        Player Add(Player player);
        void Delete(string connectionId);
        void ResetJustAdded();
    }
}
