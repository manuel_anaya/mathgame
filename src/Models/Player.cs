﻿using System.ComponentModel.DataAnnotations;

namespace MathGame.Models
{
    public class Player
    {
        [Key]
        public string ConnectionId { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }

        public bool JustAdded { get; set; }
    }
}