﻿using System;
using System.Linq;
using System.Threading.Tasks;
using MathGame.Models;
using MathGame.Repositories;
using Microsoft.AspNetCore.SignalR;

namespace MathGame.Hubs
{
    public class MathGameHub : Hub
    {
        private readonly IPlayersRepository _playersRepository;
        public MathGameHub(IPlayersRepository playersRepository)
        {
            _playersRepository = playersRepository;
        }

        public override Task OnConnectedAsync()
        {
            Clients.Client(Context.ConnectionId).SendAsync("setConnectionId", Context.ConnectionId);
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
            var player = _playersRepository.GetPlayer(Context.ConnectionId);
            if(player != null)
            {
                // remove person from game when disconnected
                _playersRepository.Delete(Context.ConnectionId);
                // let other players know
                Clients.All.SendAsync("leftGame", player.Name);
            }
            
            return base.OnDisconnectedAsync(exception);
        }

        public Task BroadcastMessage(string message)
        {
            return Clients.All.SendAsync("BroadcastMessage", message);
        }

        public Task SendDirectMessage(string connectionId, string message)
        {
            return Clients.Client(connectionId).SendAsync("SendMessage", message);
        }
    }
}
