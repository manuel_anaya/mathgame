﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MathGame.Dtos;
using MathGame.Hubs;
using MathGame.Repositories;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace MathGame.Services
{
    public class SchedulerHostedService : HostedServiceBase
    {
        private readonly ILogger<SchedulerHostedService> _logger;
        private readonly IOptions<TimerServiceConfiguration> _options;
        private readonly IHubContext<MathGameHub> _mathGameHubContext;
        private readonly IPlayersRepository _playersRepository;
        private readonly IMathChallenge _mathChallenge;
        private int _step = 1;

        private readonly Random _random = new Random();

        public SchedulerHostedService(
          ILoggerFactory loggerFactory,
          IOptions<TimerServiceConfiguration> options,
          IHubContext<MathGameHub> hubContext,
          IPlayersRepository playersRepository,
          IMathChallenge mathChallenge)
        {
            _logger = loggerFactory.CreateLogger<SchedulerHostedService>();
            _options = options;
            _mathGameHubContext = hubContext;
            _playersRepository = playersRepository;
            _mathChallenge = mathChallenge;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            var _answerSeconds = 5;
            var _totalSteps = 5;

            while (!cancellationToken.IsCancellationRequested)
            {
                if(_playersRepository.GetPlayers().Count == 0)
                {
                    await Task.Delay(5000, cancellationToken);
                    
                    // restart round variables
                    _playersRepository.GetPlayers().ForEach(x => x.Score = 0);
                    _mathChallenge.Answers.Clear();
                    _step = 1;

                    continue;
                }

                _playersRepository.ResetJustAdded();

                // reached end of the round. Reset scores, display table of positions.
                if (_step > _totalSteps)
                {
                    // show the end of round screen and show winner
                    var winner = _playersRepository.GetPlayers().OrderByDescending(x => x.Score).First();

                    _logger.LogInformation($"Round has concluded with winner {winner.Name}");

                    if (winner != null)
                        await _mathGameHubContext.Clients.All.SendAsync("concludeRound", winner.Name);

                    //additional 5 seconds to watch results and prepare for next round                    
                    for (int i = _answerSeconds; i >= 0; i--)
                    {
                        await _mathGameHubContext.Clients.All.SendAsync("updateNewRoundCountDown", i);
                        await Task.Delay(1000, cancellationToken);
                    }

                    // restart round variales
                    _playersRepository.GetPlayers().ForEach(x => x.Score = 0);                    
                    _step = 1;
                }

                // generate a new challenge and fill the singleton math challenge
                GenerateChallenge();

                // takes people out of the lobby and into the game screen
                await _mathGameHubContext.Clients.All.SendAsync("showGame");

                //indicate the progress of the current round
                await _mathGameHubContext.Clients.All.SendAsync("showStep", $"{_step}/{_totalSteps}");

                await _mathGameHubContext.Clients.All.SendAsync("displayChallenge", new { challengeId = _mathChallenge.ChallengeId, predicate = _mathChallenge.Predicate });
                _step++;

                // delta of time to allow users to submit their answers - update UI timer
                for (int i = _answerSeconds; i >= 0; i--)
                {
                    await _mathGameHubContext.Clients.All.SendAsync("updateCountDown", i);
                    await Task.Delay(1000, cancellationToken);
                }

                //all of those who didn't answer will get a minus point
                var answered = _mathChallenge.Answers.Select(x => x.Item1).ToList();
                foreach(var player in _playersRepository.GetPlayers().Where(x => !answered.Contains(x.ConnectionId) && x.JustAdded == false))
                {
                    player.Score--;
                }

                //show partial results to show players positions
                var rankings = _playersRepository.GetPlayers().OrderByDescending(x => x.Score)
                    .Select((x, index) => new RankingDto {
                        Position = index,
                        Name = x.Name,
                        Score = x.Score,
                        IsWinner = (_mathChallenge.ChallengeWinner != null && x.ConnectionId == _mathChallenge.ChallengeWinner)
                    }).ToList();

                await _mathGameHubContext.Clients.All.SendAsync("showResults", rankings);

                // show cool-down screen unless it's the finish game screen coming up
                if (_step <= _totalSteps)
                {
                    // show cooldown before starting next round
                    await _mathGameHubContext.Clients.All.SendAsync("showCoolDown");
                    for (int i = _answerSeconds - 1; i >= 0; i--)
                    {
                        await _mathGameHubContext.Clients.All.SendAsync("showCoolDownCountDown", i);
                        await Task.Delay(1000, cancellationToken);
                    }
                }                
            }
        }

        private void GenerateChallenge()
        {
            var random = new Random();
            var operand1 = random.Next(1, 10);
            var operand2 = random.Next(1, 10);

            // 50% chance to produce a real result
            var isTrue = random.Next(2) == 1;
            // string composition for the challenge display
            var predicate = string.Empty;
            //declared as double to support division, later casted as string
            double result = 0;

            //generates a number between 0 and 4
            switch (random.Next(4))
            {
                // addition
                case 0:
                    result = isTrue ? operand1 + operand2 : GetNewRandom(operand1) + operand2;
                    predicate = $"{operand1} + {operand2} = {result}";
                    break;
                // substraction
                case 1:
                    result = isTrue ? operand1 - operand2 : GetNewRandom(operand1) - operand2;
                    predicate = $"{operand1} - {operand2} = {result}";
                    break;
                // multiplication
                case 2:
                    result = isTrue ? operand1 * operand2 : GetNewRandom(operand1) * operand2;
                    predicate = $"{operand1} * {operand2} = {result}";
                    break;
                // division
                default:
                    result = isTrue ? (operand1 / (double)operand2) : (GetNewRandom(operand1) / (double)operand2);
                    predicate = $"{operand1} / {operand2} = {result.ToString("0.0")}";
                    break;
            }

            _mathChallenge.RefreshChallenge(id: Guid.NewGuid().ToString(), predicate: predicate, rightAnswer: isTrue);
        }

        //ensures gets a new random number distinct than the originally got
        private int GetNewRandom(int chosen)
        {
            var random = new Random();
            var newNumber = random.Next(1, 10);
            while(newNumber == chosen)
            {
                newNumber = random.Next(1, 10);
            }
            return newNumber;
        }
    }
}
