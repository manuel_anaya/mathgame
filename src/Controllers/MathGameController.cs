﻿using System;
using System.Linq;
using MathGame.Hubs;
using MathGame.Dtos;
using AutoMapper;
using MathGame.Models;
using MathGame.Repositories;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;

namespace MathGame.Controllers
{
    [Route("api/[controller]")]
    public class MathGameController : Controller
    {
        private readonly ILogger<MathGameController> _logger;
        private readonly IPlayersRepository _playersRepository;
        private readonly IHubContext<MathGameHub> _mathGameHubContext;
        private readonly IMathChallenge _mathChallenge;

        //todo: find a way to stop execution from service
        private bool _continueGame = true;
        private static int _step = 1;

        public MathGameController(IPlayersRepository playersRepository, IHubContext<MathGameHub> mathGameHubContext, IMathChallenge mathChallenge, ILogger<MathGameController> logger)
        {            
            _playersRepository = playersRepository;
            _mathGameHubContext = mathGameHubContext;
            _logger = logger;
            _mathChallenge = mathChallenge;
        }

        [HttpGet]
        public IActionResult IsConnected(string playerName)
        {
            return Ok(_playersRepository.GetPlayers().Any(x => x.Name == playerName));
        }

        [HttpPost]
        public IActionResult Answer([FromBody] PlayerAnswerDto viewModel)
        {
            if (viewModel == null || !ModelState.IsValid)
            {
                var info = viewModel == null ?
                    "Null viewModel" :
                    string.Join(";", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                _logger.LogError($"Failed to Join game. Error info: {info}");
                return BadRequest();
            }

            if (_playersRepository.GetPlayer(viewModel.ConnectionId) != null && _mathChallenge.ChallengeId == viewModel.ChallengeId)
            {
                lock (_playersRepository)
                {
                    lock (_mathChallenge)
                    {
                        // if already has answered to this challenge then skip
                        if (_mathChallenge.Answers.Any(x => x.Item1 == viewModel.ConnectionId && x.Item2 == viewModel.ChallengeId))
                        {
                            _mathGameHubContext.Clients.Client(viewModel.ConnectionId).SendAsync("alreadyAswered", "You have already answered");
                            return BadRequest("You have already answered");
                        }

                        // register the answer
                        _mathChallenge.Answers.Add(new Tuple<string, string, bool>(viewModel.ConnectionId, viewModel.ChallengeId, viewModel.Answer));

                        var roundWinner = _playersRepository.GetPlayer(viewModel.ConnectionId);

                        // if provided right answer
                        if (_mathChallenge.RightAnswer == viewModel.Answer)
                        {
                            // first person to submit the right answer
                            if (string.IsNullOrEmpty(_mathChallenge.ChallengeWinner))
                            {
                                //set challenge Winner
                                _mathChallenge.ChallengeWinner = viewModel.ConnectionId;
                                _mathGameHubContext.Clients.Client(viewModel.ConnectionId).SendAsync("answerResponse", "You got the right answer first! +1 points");
                                // increase score by 1
                                roundWinner.Score++;
                            }
                            else
                                _mathGameHubContext.Clients.Client(viewModel.ConnectionId).SendAsync("answerResponse", "Correct answer but someone has submitted it before you +0 points");
                        }
                        else
                        {
                            roundWinner.Score--;
                            _mathGameHubContext.Clients.Client(viewModel.ConnectionId).SendAsync("answerResponse", "Sorry, this answer is incorrect -1 points");
                        }

                        return Ok("Answer delivered");
                    }
                }

            }

            return BadRequest("Invalid challenge");

        }

        [HttpPut]
        public IActionResult Join([FromBody] PlayerDto viewModel)
        {
            if (viewModel == null || !ModelState.IsValid)
            {
                var info = viewModel == null ? 
                    "Null viewModel" : 
                    string.Join(";", ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage));
                _logger.LogError($"Failed to Join game. Error info: {info}");
                return BadRequest();
            }

            Player player = Mapper.Map<Player>(viewModel); 

            //todo: show a waiting screen loading for example
            if(_playersRepository.GetPlayers().Count >= 10)
            {
                return Ok("fullRoom");
            }
            else if (_playersRepository.GetPlayers().Any(x => string.Equals(x.Name.Trim(), player.Name.Trim(), StringComparison.InvariantCultureIgnoreCase)))
            { // chosen name has already been taken                
                return Ok("nameTaken");
            }

            // register player
            // JustAdded: in case new player joins mid-game, we give a startup point so it doesn't get an initial penalty while in the lobby
            _playersRepository.Add(new Player { Name = player.Name, Score = 0, ConnectionId = player.ConnectionId, JustAdded = true });
            
            // inform other players about new challenger
            _mathGameHubContext.Clients.AllExcept(new[]{player.ConnectionId}).SendAsync("newChallenger", player.Name);
            
            return Ok("joinLobby");
        }       
    }
}