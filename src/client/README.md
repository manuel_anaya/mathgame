# Browser-based Math game
Simple math game where players compete concurrently to solve faster the predicate on screen. 
* The first player to answer correctly gets + 1 point
* correct answers after a person answered correctly will give no points.
* No answer or incorrect answers will have a penalty of -1 point.
* Match consists of 5 rounds where the winner will be shown and a new game will start

### Technologies used:
* ASP.NET Core
* SignalR Client
* Angular (Typescript)
* Bootstrap

### Installation
* Clone the repository or download the source
* Navigate to the ***src\MathGame\client*** game folder and run
```javascript
npm install
```
* After restoring the project packages run 
```javascript
npm run build
```

### Running the project
While running the project using .NET Core command: ***dotnet run*** sometimes I found some issues regarding permissions for the Kestrel WebServer ports access (permission denied) while working concurrently. This happened when running several pages in my local machine. I would recommend using Visual Studio to run the several clients in the same machine. 

* Option 1: Open Visual Studio and open the solution file ***MathGame.sln*** located in the root folder. Press F5 or press Run. Be sure all packages are restored and project built using the steps above. This will start the project in the ***port 5000***

* Option 2: in the client folder, run 
```
npm run start
```
This will use "concurrently* package to run the dotnet core application after performing a build. It will run in the ***port 4200***
