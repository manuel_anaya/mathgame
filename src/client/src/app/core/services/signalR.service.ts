import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { Ranking } from '../../models/ranking.model';
import { MathChallege } from '../../models/mathChallenge.model';
import { CONFIGURATION } from '../../shared/app.constants';

@Injectable()
export class SignalRService {
  mathGameChanged = new EventEmitter<string>();
  connectionEstablished = new EventEmitter<Boolean>();
  connectionIdSet = new EventEmitter<string>();
  onJoinLobby = new EventEmitter();
  newChallengerJoined = new EventEmitter<string>();
  onShowGame = new EventEmitter<string>();
  onUpdateNewRoundCountDown = new EventEmitter<Number>();
  onShowStep = new EventEmitter<string>();
  onDisplayChallenge = new EventEmitter<MathChallege>();
  onUpdateCountDown = new EventEmitter<Number>();
  onShowResults = new EventEmitter<Ranking[]>();
  onAnswerResponse = new EventEmitter<string>();
  onShowCoolDown = new EventEmitter();
  onConcludeRound = new EventEmitter<string>();
  onShowCoolDownCountDown = new EventEmitter<Number>();

  private connectionIsEstablished = false;
  private _hubConnection: HubConnection;

  constructor() {
    this.createConnection();
    this.registerOnServerEvents();
    this.startConnection();
  }

  private createConnection() {
    this._hubConnection = new HubConnectionBuilder()
      .withUrl(CONFIGURATION.baseUrls.server + 'mathgamehub')
      .build();
  }

  private startConnection(): void {
    this._hubConnection
      .start()
      .then(() => {
        this.connectionIsEstablished = true;
        this.connectionEstablished.emit(true);

      })
      .catch(err => {
        setTimeout(this.startConnection(), 5000);
      });
  }

  private registerOnServerEvents(): void {
    this._hubConnection.on('setConnectionId', (data: string) => {
      this.connectionIdSet.emit(data);
    });

    this._hubConnection.on('joinLobby', () => {
      this.onJoinLobby.emit();
    });

    this._hubConnection.on('newChallenger', (data: string) => {
      this.newChallengerJoined.emit(data);
    });

    this._hubConnection.on('updateNewRoundCountDown', (data: Number) => {
      this.onUpdateNewRoundCountDown.emit(data);
    });

    this._hubConnection.on('showGame', () => {
      this.onShowGame.emit();
    });

    this._hubConnection.on('showStep', (data: string) => {
      this.onShowStep.emit(data);
    });

    this._hubConnection.on('displayChallenge', (data: MathChallege) => {
      this.onDisplayChallenge.emit(data);
    });

    this._hubConnection.on('updateCountDown', (data: Number) => {
      this.onUpdateCountDown.emit(data);
    });

    this._hubConnection.on('showResults', (data: Ranking[]) => {
      this.onShowResults.emit(data);
    });

    this._hubConnection.on('answerResponse', (data: string) => {
      this.onAnswerResponse.emit(data);
    });

    this._hubConnection.on('showCoolDown', () => {
      this.onShowCoolDown.emit();
    });

    this._hubConnection.on('showCoolDownCountDown', (data: Number) => {
      this.onShowCoolDownCountDown.emit(data);
    });

    this._hubConnection.on('concludeRound', (data: string) => {
      this.onConcludeRound.emit(data);
    });

  }
}
