import {
  HttpClient,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Headers, Response } from '@angular/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Player } from '../../models/player.model';
import { PlayerAnswer } from '../../models/player.answer.model';
import { CONFIGURATION } from '../../shared/app.constants';

@Injectable()
export class MathGameDataService {
  private actionUrl: string;
  private headers = new Headers();

  onJoinProcessed = new EventEmitter<string>();
  onIsConnected = new EventEmitter<boolean>();
  onAnswered = new EventEmitter<string>();

  constructor(private _http: HttpClient) {
    this.actionUrl =
      CONFIGURATION.baseUrls.server +
      CONFIGURATION.baseUrls.apiUrl +
      'mathgame/';
  }


  // use the angular httpclient to make a post to join the new player
  public join(player: Player): Observable<Object> {
    const toAdd: string = JSON.stringify({ Name: player.name, ConnectionId: player.connectionId });
    return this._http
      .put<Player>(this.actionUrl, toAdd)
      .pipe(
        tap(response=> this.processJoinResponse(response.toString())),
        catchError(this.handleError)
      );
  }

  public answer(player: PlayerAnswer): Observable<Object> {
    const toAdd: string = JSON.stringify({ ConnectionId: player.connectionId, ChallengeId: player.challengeId, Answer: player.answer });
    return this._http
      .post<Player>(this.actionUrl, toAdd)
      .pipe(
        tap(response => this.processAnswered(response.toString())),
        catchError(this.handleError)
      );
  }

  public isConnected(playerName: string): Observable<Object> {
    const toCheck: string = JSON.stringify({ playerName: playerName });
    return this._http.get<string>(this.actionUrl + playerName)
      .pipe(
      tap(response => this.processIsConnectedResponse(response.toString())),
      catchError(this.handleError)
      );
  }

  // once we request a join game, we will emit an event with the response message
  private processJoinResponse(response: string) {
    this.onJoinProcessed.emit(response);
  }

  private processIsConnectedResponse(response: string) {
    this.onIsConnected.emit(response == 'true');
  }

  private processAnswered(response: string) {   
    this.onAnswered.emit(response);
  }

  private handleError(error: Response) {
    console.dir(error);
    return throwError(error || 'Server error');
  }

}

@Injectable()
export class MathGameHttpInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log(JSON.stringify(req));

    if (!req.headers.has('Content-Type')) {
      req = req.clone({
        headers: req.headers.set('Content-Type', 'application/json')
      });
    }

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    return next.handle(req);
  }
}
