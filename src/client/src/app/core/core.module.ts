import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { MathGameDataService, MathGameHttpInterceptor } from './services/mathgame-data.service';
import { SignalRService } from './services/signalR.service';

@NgModule({
  imports: [HttpClientModule],
  exports: [],
  declarations: [],
  providers: []
})
export class CoreModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule,
      providers: [
        MathGameDataService,
        SignalRService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: MathGameHttpInterceptor,
          multi: true
        }
      ]
    };
  }
}
