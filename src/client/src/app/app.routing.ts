import { RouterModule, Routes } from '@angular/router';
import { MathgameComponent } from './home/mathgame/mathgame.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'mathgame', pathMatch: 'full' },
  { path: 'mathgame', component: MathgameComponent }
];

export const appRouting = RouterModule.forRoot(appRoutes, { useHash: true });
