import { Component, Input } from '@angular/core';
import { PlayGameComponent } from './play-game.component';
import { Ranking } from '../../../models/ranking.model';

@Component({
  selector: 'ranking-results',
  template: `
    <div class="d-flex justify-content-center">
      <div class="d-flex flex-column col-md-6">
        <table class="table table-sm">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Points</th>
            </tr>
          </thead>
          <tbody>
            <tr *ngFor="let ranking of rankings" ng-class="{table-success:ranking.isWinner}">
              <th scope="row">{{ranking.position}}</th>
              <td>{{ranking.name}}</td>
              <td>{{ranking.score}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  `
})

export class RankingResultsComponent {
  constructor(private playGame: PlayGameComponent) { }
  @Input() rankings: Ranking[];
}
