import { Component, EventEmitter, Input, Output, SimpleChanges, OnInit } from '@angular/core';
import { MathgameComponent } from '../mathgame.component';
import { Ranking } from '../../../models/ranking.model';

@Component({
    selector: 'app-play-game',
    templateUrl: './play-game.component.html',
    styleUrls: ['./play-game.component.css']
})

export class PlayGameComponent implements OnInit {

  constructor(private mathComponent: MathgameComponent) {}
  @Input() showCoolDown: boolean;
  @Input() hasAnswered: boolean;
  @Input() playerName: string;
  @Input() predicate: string;
  @Input() stepNumber: string;
  @Input() countDown: Number;
  @Input() showCoolDownCountDown: Number;
  @Input() showCooldown: boolean;
  @Input() rankings: Ranking[];
  @Input() isConcluded: boolean;
  @Input() winnerName: string;
  @Input() newRoundCountDown: Number;
  @Output() emitChosenAnswer: EventEmitter<boolean> = new EventEmitter();

  ngOnInit() {
    this.mathComponent.onNewChallenge.subscribe(() => {
      this.showCoolDown = false;
      this.hasAnswered = false;
      this.isConcluded = false;
    });

    this.mathComponent.onShowCoolDown.subscribe(() => {
      this.showCoolDown = true;
    });
  }  

  chooseAnswer(answer: boolean) {
    this.hasAnswered = true;
    this.emitChosenAnswer.emit(answer);
  }

}
