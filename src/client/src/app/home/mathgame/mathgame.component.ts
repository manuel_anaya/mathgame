import { Component, EventEmitter, OnInit } from '@angular/core';
import { SignalRService } from '../../core/services/signalR.service';
import { MathGameDataService } from '../../core/services/mathgame-data.service';
import { Player } from '../../models/player.model';
import { PlayerAnswer } from '../../models/player.answer.model';
import { MathChallege } from '../../models/mathChallenge.model';
import { ToastrService } from 'ngx-toastr';
import { Ranking } from '../../models/ranking.model';

@Component({
    selector: 'app-mathgame',
    templateUrl: './mathgame.component.html',
    styleUrls: ['./mathgame.component.css']
})

export class MathgameComponent implements OnInit {

  toasterSettings: Object = {
    timeOut: 3000,
    positionClass: 'toast-bottom-right',
    tapToDismiss: true,
    progressBar: true
  };

  regex: RegExp = /^[a-zA-Z0-9.\-_$@*!]{3,30}$/;
  isConnectionEstablished: boolean = false;
  message: string = '';
  hasJoined: boolean = false;
  connectionId: string = '';
  challengeId: string = '';
  validationMessage: string = '';
  playerName: string = '';
  inLobby: boolean = true;
  predicate: string = '';
  hasAnswered: boolean = null;
  countDown: Number = null;
  stepNumber: string = '';
  showCooldown: boolean = false;
  showCoolDownCountDown: Number = 0;
  rankings: Ranking[] = [];
  isConcluded: boolean = false;
  winnerName: string = '';
  newRoundCountDown: Number = 0;

  onNewChallenge = new EventEmitter();
  onShowCoolDown = new EventEmitter();

  constructor(
    private signalRService: SignalRService,
    private dataService: MathGameDataService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    //this.subscribeToEvents();
    // once connection is established with the hub, it will send back the connectionId
    this.signalRService.connectionEstablished.subscribe(() => {
      this.isConnectionEstablished = true;
    });

    //fills the connection id from the Hub's context
    this.signalRService.connectionIdSet.subscribe((id: string) => {
      this.connectionId = id;
    });

    this.dataService.onJoinProcessed.subscribe((response: string) => {
      if (response == 'fullRoom') {
        this.validationMessage = 'The game is currently full, please try again later.';
      }
      else if (response == 'nameTaken') {
        this.validationMessage = 'This name has been already taken, please type a new one.';
      }
      else if (response == 'joinLobby') {
        this.hasJoined = true;
        this.inLobby = true;
      }
    });

    this.dataService.onIsConnected.subscribe((response: boolean) => {
      this.hasJoined = response;
    });

    // handle case of reconnection where user is still in the players list => send back the players dto?
    this.signalRService.onJoinLobby.subscribe(() => {
      this.hasJoined = true;
      this.inLobby = true;      
    });

    this.signalRService.onShowGame.subscribe(() => {
      if (this.hasJoined == true) {
        this.inLobby = false;
        this.showCooldown = false;
        this.isConcluded = false;
        this.onNewChallenge.emit();
      }      
    });

    this.signalRService.onDisplayChallenge.subscribe((data: MathChallege) => {
      this.hasAnswered = false;
      this.predicate = data.predicate;
      this.challengeId = data.challengeId;
      this.onNewChallenge.emit();
    });

    this.signalRService.onUpdateCountDown.subscribe((data: Number) => {
      this.countDown = data;
    });

    this.signalRService.onShowStep.subscribe((data: string) => {
      this.stepNumber = data;
    });

    this.signalRService.onAnswerResponse.subscribe((data: string) => {
      if (data.includes('+1')) {

        this.showSuccess(data);
      }
      else if (data.includes('-1')) {
        this.showError(data);
      }
      else {
        this.showInfo(data);
      }      
    });

    this.signalRService.onShowCoolDown.subscribe(() => {
      this.showCooldown = true;
      this.onShowCoolDown.emit();
    });

    this.signalRService.onShowCoolDownCountDown.subscribe((data: Number) => {
      this.showCoolDownCountDown = data;
    });

    this.signalRService.onShowResults.subscribe((data: Ranking[]) => {
      this.rankings = data;
    });

    this.signalRService.onConcludeRound.subscribe((data) => {
      this.isConcluded = true;
      this.inLobby = false;
      this.showCooldown = false;
      this.winnerName = data;
    });

    this.signalRService.onUpdateNewRoundCountDown.subscribe((data: Number) => {
      this.newRoundCountDown = data;
    });

  

    this.signalRService.newChallengerJoined.subscribe((name: string) => {
      this.showInfo('Challenger ' + name + ' has joined the game!');      
    });
  }

  join(playerName: string) {
    // reset validation message
    this.validationMessage = '';

    if (playerName === null || playerName.length === 0 || !this.regex.test(playerName)) {
      this.validationMessage = 'Please enter a valid username';
      return;
    }

    if (this.connectionId) {
      const player = new Player()
      player.name = playerName;
      player.connectionId = this.connectionId;      
      
      this.dataService
      .join(player)
      .subscribe((data) => {
          const response = data.toString();
          if(data !== 'joinLobby')
          {
            if(data === 'fullRoom'){
              this.validationMessage = 'Currently this game session is full, please try again later.';
            }
            if(data === 'nameTaken'){
              this.validationMessage = 'The name you have chosen is already taken, please choose a new one.';
            }
          }
          else{
            this.playerName = player.name;
          }
        }, error => {
          this.showError(error);
        });
    }
    else {
      this.showError('Error retrieving connection id');      
    }
  }

  processAnswer(answer: boolean) {
    this.hasAnswered = true;
    const playerAnswer = new PlayerAnswer();
    playerAnswer.connectionId = this.connectionId;
    playerAnswer.answer = answer;
    playerAnswer.challengeId = this.challengeId;
    this.dataService.answer(playerAnswer).subscribe((data) => { }, error => { this.showError });
  }

  showInfo(message) {
    this.toastr.info(message, null, this.toasterSettings);
  }

  showError(message) {
    this.toastr.error(message, null, this.toasterSettings);
  }

  showSuccess(message) {
    this.toastr.success(message, null, this.toasterSettings);
  }

  //private subscribeToEvents(): void {
    //this.signalRService.mathGameChanged.subscribe((message: string) => {
    //  this._ngZone.run(() => {
    //    this.message = message;
    //  });
    //});
  //}

}
