import { Component, EventEmitter, Input, Output  } from '@angular/core';
import { Player } from '../../../models/player.model';

@Component({
    selector: 'app-join-game',
    templateUrl: './join-game.component.html',
    styleUrls: ['./join-game.component.css']
})

export class JoinGameComponent {
  playerName: string = '';

  @Input() validationMessage: string;
  @Input() connectionId:string;
  @Output() clickedJoin: EventEmitter<string> = new EventEmitter();

  joinGame(playerName: string) {
    this.clickedJoin.emit(this.playerName); 
  }
}
