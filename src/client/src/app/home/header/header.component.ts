import { Component } from '@angular/core';

@Component({
  selector: 'app-header',
  template:
    `<div class="App">
        <div class="App-header">
          <div style="height: 80px;">
            <img src="assets/images/calculator.png" class="App-logo" alt="Calculator" />
            <h3>Welcome to the Math Game</h3>
          </div>
        </div>
    </div>`,
    styleUrls: ['./header.component.css']
})

export class HeaderComponent {

}
