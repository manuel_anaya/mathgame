import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { MathgameComponent } from './mathgame/mathgame.component';
import { FooterComponent } from './footer/footer.component';
import { JoinGameComponent } from './mathgame/join-game/join-game.component';
import { PlayGameComponent } from './mathgame/play-game/play-game.component';
import { RankingResultsComponent } from './mathgame/play-game/ranking-results.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  exports: [],
  declarations: [
    HeaderComponent,
    FooterComponent,
    MathgameComponent,
    JoinGameComponent,
    PlayGameComponent,
    RankingResultsComponent
  ],
  providers: []
})
export class HomeModule {}
