import { Component } from '@angular/core';

@Component({
  selector: 'app-footer',
  template:
      `<div class="footer">
        <p>Test task presented by <strong>Manuel Anaya</strong></p>
      </div>`,
    styleUrls: ['./footer.component.css']
})
/** footer component*/
export class FooterComponent {
    /** footer ctor */
    constructor() {

    }
}
