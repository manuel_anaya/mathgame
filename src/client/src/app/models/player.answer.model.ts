export class PlayerAnswer {
  connectionId: string = '';
  challengeId: string = '';
  answer: boolean = null; 
}
