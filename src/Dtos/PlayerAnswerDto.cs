﻿namespace MathGame.Dtos
{
    public class PlayerAnswerDto
    {
        public string ConnectionId { get; set; }

        public string ChallengeId { get; set; }

        public bool Answer { get; set; }
    }
}
