﻿namespace MathGame.Dtos
{
    public class RankingDto
    {
        public int Position { get; set; }
        public string Name { get; set; }
        public int Score { get; set; }
        public bool IsWinner { get; set; }
    }
}
