﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MathGame.Dtos
{
    public class PlayerDto
    {
        public string ConnectionId { get; set; }

        public string Name { get; set; }

        public int Score { get; set; }
    }
}
